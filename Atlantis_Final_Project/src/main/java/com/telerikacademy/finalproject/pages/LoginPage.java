package com.telerikacademy.finalproject.pages;

public class LoginPage extends BasePage {
    public LoginPage() {
        super("login.url");
    }
    public final String loginButton = "login.LoginButton";

    public void login(String userName, String password){
        actions.waitForElementVisible("login.Username", 30);
        actions.typeValueInField(userName, "login.Username");
        actions.waitMillis(500);
        actions.waitForElementVisible("login.Password", 30);
        actions.typeValueInField(password, "login.Password");
        actions.waitMillis(500);
        actions.clickElement("login.LoginButton");
        actions.waitMillis(500);
    }
}