package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;

public class UpdateUserProfilePage extends BasePage {

    public UpdateUserProfilePage() {
        super("profile.url");
    }

    public void openEditProfile(){
        actions.waitForElementVisible("navigation.Profile",30);
        actions.moveToElement("navigation.Profile");
        actions.waitForElementVisible("profile.EditButton", 30);
        actions.clickElement("profile.EditButton");
        actions.waitMillis(500);
    }

    public void editProfilePictureVisibilityToConnections(){
        actions.waitForElementVisible("profile.VisibilityMenu", 30);
        actions.clickElement("profile.VisibilityMenu");
        actions.waitForElementVisible("profile.VisibilityConnections", 30);
        actions.clickElement("profile.VisibilityConnections");
    }

    public void saveChanges(){
        actions.waitForElementVisible("button.Save", 30);
        actions.clickElement("button.Save");
        actions.waitMillis(500);
    }

    public void assertChangeVisibility(){
        openEditProfile();
        actions.waitForElementVisible("profile.VisibilityConnectionsSelected", 30);
        actions.assertElementPresent("profile.VisibilityConnectionsSelected");
        saveChanges();
    }

    public void enterNewLastName(String newName){
        actions.waitForElementVisible("profile.inputLastName", 30);
        actions.clearTextField("profile.inputLastName");
        actions.waitMillis(500);
        actions.typeValueInField(UserActions.getTextWithKey(newName), "profile.inputLastName");
    }
    public void assertChangeLastName(String name){
        actions.assertTextExists(UserActions.getTextWithKey(name));
    }

//    public void clickOnChangePasswordButton(){
//        actions.waitForElementVisible("profile.ChangePasswordButton", 30);
//        actions.clickElement("profile.ChangePasswordButton");
//        actions.waitMillis(500);
//    }
//
//    public void changePassword(String oldPass, String newPass){
//        actions.waitForElementVisible("profile.inputOldPassword", 30);
//        actions.typeValueInField(UserActions.getTextWithKey(oldPass),"profile.inputOldPassword");
//        actions.waitMillis(500);
//        actions.typeValueInField(UserActions.getTextWithKey(newPass),"profile.inputNewPassword");
//        actions.waitMillis(500);
//        actions.typeValueInField(UserActions.getTextWithKey(newPass),"profile.inputConfirmPassword");
//        actions.waitMillis(500);
//    }
//
//    public void assertChangePassword(){
//        actions.waitForElementVisible("navigation.Logout",30);
//        actions.assertElementPresent("navigation.Logout");
//    }

    public void clearData(){
        openEditProfile();
        actions.waitForElementVisible("profile.VisibilityMenu", 30);
        actions.clickElement("profile.VisibilityMenu");
        actions.waitForElementVisible("profile.VisibilityPublic", 30);
        actions.clickElement("profile.VisibilityPublic");
        actions.waitForElementVisible("profile.inputLastName", 30);
        actions.clearTextField("profile.inputLastName");
        actions.typeValueInField(UserActions.getTextWithKey("update.LastName"), "profile.inputLastName");
        saveChanges();
//        LogoutPage logout = new LogoutPage();
//        logout.logout();
//        LoginPage login = new LoginPage();
//        login.login(UserActions.getTextWithKey("regularUser.username"), UserActions.getTextWithKey("update.newPassword"));
//        openEditProfile();
//        clickOnChangePasswordButton();
//        actions.waitForElementVisible("profile.inputOldPassword", 30);
//        actions.typeValueInField(UserActions.getTextWithKey("update.newPassword"),"profile.inputOldPassword");
//        actions.typeValueInField(UserActions.getTextWithKey("regularUser.password"),"profile.inputNewPassword");
//        actions.typeValueInField(UserActions.getTextWithKey("regularUser.password"),"profile.inputConfirmPassword");
//        saveChanges();
    }



}
