package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class UpdateNationalityPage extends BasePage {
    public UpdateNationalityPage() {
        super("updateNationalityPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }
    public void editNationality() {
        actions.clickElement("editNationality.Button");
        actions.waitMillis(WAIT_TIME);
        actions.clickElement("nationalityDelete.Button");
        actions.waitMillis(WAIT_TIME);

    }
}
