package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class DeletePostPage extends BasePage {
    public DeletePostPage() {
        super("deletePostPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }

    public void deletePost(){
        actions.waitForElementVisible("postDeleteYes.Button",30);
        actions.clickElement("postDeleteYes.Button");
        actions.waitMillis(500);
    }
}