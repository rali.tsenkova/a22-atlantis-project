package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.pages.BasePage;
import org.junit.Assert;

public class UpdateCategoryPage extends BasePage {
    public UpdateCategoryPage() {
        super("updateCategoryPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }

    public void editCategory() {
        actions.clickElement("editCategory.Button");
        actions.waitMillis(WAIT_TIME);
        actions.clickElement("deleteCategory.Button");
        actions.waitMillis(WAIT_TIME);

    }
}