package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class DeleteCommentPage extends BasePage {
    public DeleteCommentPage() {
        super("deleteCommentPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }
    public void confirmDeleteComment(){
        actions.waitForElementVisible("commentDeleteYes.Button",30);
        actions.clickElement("commentDeleteYes.Button");
        actions.waitMillis(500);
    }
    public void cancelToConfirmDeleteComment(){
        actions.waitForElementVisible("commentDeleteNo.Button",30);
        actions.clickElement("commentDeleteNo.Button");
        actions.waitMillis(500);

    }
}