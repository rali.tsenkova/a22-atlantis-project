package com.telerikacademy.finalproject.pages;

public class CategoriesAdminPage extends BasePage {
    public CategoriesAdminPage() {
        super("adminCategories.url");
    }
    public void clickAddCategory(){
        actions.clickElement("addCategory.Button");
        actions.waitMillis(WAIT_TIME);

    }
}