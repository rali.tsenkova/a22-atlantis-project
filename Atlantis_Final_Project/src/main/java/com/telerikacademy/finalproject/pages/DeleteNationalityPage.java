package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class DeleteNationalityPage extends BasePage {
    public DeleteNationalityPage() {
        super("deleteNationalityPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }

    public void deleteNationality(){
        actions.clickElement("deleteNationalityYes.Button");
        actions.waitMillis(WAIT_TIME);
    }
}