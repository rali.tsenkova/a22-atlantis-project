package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;

public class CategoryNewPage extends BasePage {
    public CategoryNewPage() {
        super("adminCategoriesNew.url");
    }

    public void createCategory(String image, String categoryName){
        String imagePath = Utils.getConfigPropertyByKey(UserActions.IMAGE_PATH);
        String fullImagePath = imagePath + image;
        actions.typeValueInField(fullImagePath, "chooseFile.Input");
        actions.waitMillis(WAIT_TIME);
        actions.typeRandomValueInField(categoryName, "inputCategory.Field");
        actions.waitMillis(WAIT_TIME);
        actions.clickElement("addCategorySave.Button");
    }
}
