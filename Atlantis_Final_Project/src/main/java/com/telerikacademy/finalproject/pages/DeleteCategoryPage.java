package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class DeleteCategoryPage extends BasePage {
    public DeleteCategoryPage() {
        super("deleteCategoryPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }

    public void deleteCategory(){
        actions.clickElement("deleteCategoryYes.Button");
        actions.waitMillis(WAIT_TIME);
    }
}