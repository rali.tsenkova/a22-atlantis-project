package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class UpdatePostPage extends BasePage {
    public UpdatePostPage() {
        super("updatePostPage");
    }

    @Override
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.matches(url));
    }
    public void clickDeletePost(){
        actions.clickElement("postDelete.Button");
        actions.waitMillis(500);
    }
}