package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;

public class PostFunctionalitiesPage extends BasePage {

    public PostFunctionalitiesPage() {
        super("posts.url");
    }

    public void openAllPostSection(){
        actions.moveToElement("navigation.LatestPosts");
        actions.clickElement("navigation.AllPosts");
        actions.waitMillis(500);
    }

    public void searchForPostByTitle(String title){
        actions.waitForElementVisible("search.Input", 30);
        actions.typeValueInField(UserActions.getTextWithKey(title),"search.Input");
        actions.waitMillis(500);
        actions.clickElement("search.Button");
        actions.waitMillis(500);
    }

    public void assertPostFound(String title){
        actions.assertTextExists(UserActions.getTextWithKey(title));
        actions.waitMillis(500);
    }

    public void openPost(){
        actions.waitForElementVisible("postSearch.firstElement", 30);
        actions.clickElement("postSearch.firstElement");
        actions.waitMillis(500);
    }

    public void likePost(){
        actions.waitForElementVisible("post.LikeButton", 30);
        actions.clickElement("post.LikeButton");
        actions.waitMillis(500);
    }

    public void dislikePost(){
        actions.waitForElementVisible("post.DislikeButton", 30);
        actions.clickElement("post.DislikeButton");
        actions.waitMillis(500);
    }

    public void assertPostLiked(){
        actions.assertElementPresent("post.DislikeButtonActive");
        actions.waitMillis(500);
    }

    public void assertPostDisliked(){
        actions.assertElementPresent("post.LikeButtonActive");
        actions.waitMillis(500);
    }

    public void leaveComment(String comment){
        actions.waitForElementVisible("comment.Input", 30);
        actions.typeValueInField(UserActions.getTextWithKey(comment), "comment.Input");
        actions.waitMillis(500);
        actions.clickElement("comment.SendButton");
        actions.waitMillis(500);
    }

    public void assertCommentLeft(String comment){
        actions.assertTextExists(UserActions.getTextWithKey(comment));
        actions.waitMillis(500);
    }
}
