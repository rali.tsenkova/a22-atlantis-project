package com.telerikacademy.finalproject.pages;

public class NationalitiesAdminPage extends BasePage {
    public NationalitiesAdminPage() {
        super("adminNationalities.url");
    }
    public void addNationality(String nationalityName){
        actions.clickElement("adminNationalitiesAdd.Button");
        actions.waitMillis(WAIT_TIME);
        actions.typeRandomValueInField(nationalityName, "inputNationality");
        actions.clickElement("nationalitySave.Button");
        actions.waitMillis(WAIT_TIME);
    }
}