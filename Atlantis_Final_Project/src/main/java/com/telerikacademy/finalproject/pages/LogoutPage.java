package com.telerikacademy.finalproject.pages;

public class LogoutPage extends BasePage {
    public LogoutPage() {
        super("logout.url");
    }
    public final String loginButton = "login.LoginButton";
    public void logout(){
        actions.clickElement("navigation.Logout");
        actions.waitMillis(500);
    }
}