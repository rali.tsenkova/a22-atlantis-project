package com.telerikacademy.finalproject.pages;

public class PostsAdminPage extends BasePage {
    public PostsAdminPage() {
        super("adminPosts.url");
    }
    public void clickPostWithComment(){
        actions.clickElement("sortComments.Button");
        actions.waitMillis(WAIT_TIME);
        actions.clickElement("postWithComment");
        actions.waitMillis(WAIT_TIME);
    }
    public void clickOnPost(){
        actions.clickFirstElement("postAdmin");
        actions.waitMillis(500);
    }
}
