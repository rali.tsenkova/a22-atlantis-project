package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;

public class UsersFunctionalitiesPage extends BasePage {

    public UsersFunctionalitiesPage() {
        super("users.url");
    }

    public void openAllUsersSection(){
        actions.moveToElement("navigation.Users");
        actions.clickElement("navigation.AllUsers");
        actions.waitMillis(500);
    }

    public void searchForUserByEmail(String email){
        actions.typeValueInField(UserActions.getTextWithKey(email),"search.Input");
        actions.clickElement("search.Button");
        actions.waitMillis(500);
    }

    public void assertUserFound(String mail){
        actions.waitForElementVisible("users.Content", 30);
        actions.waitMillis(500);
        actions.assertTextExists(UserActions.getTextWithKey(mail));
        actions.waitMillis(500);
    }

    public void openUserProfile(){
        actions.waitForElementVisible("users.FoundedUser", 30);
        actions.clickElement("users.FoundedUser");
        actions.waitMillis(500);
    }

    public void sendConnectionRequest(){
        actions.waitForElementVisible("users.ConnectButton", 30);
        actions.clickElement("users.ConnectButton");
        actions.waitMillis(500);
    }

    public void assertRequestSent(){
        actions.waitForElementVisible("users.ConnectButton", 30);
        actions.assertTextExists("SENT REQUEST (REJECT)");
        actions.waitMillis(500);
        //unsent request
        sendConnectionRequest();
        actions.waitMillis(500);
    }

}
