package com.telerikacademy.finalproject.pages;


import com.telerikacademy.finalproject.utils.UserActions;

public class CreatePostPage extends BasePage {

    public CreatePostPage() {
        super("newPost.url");
    }

    public void openCreateNewPost(){
        actions.waitForElementVisible("navigation.LatestPosts", 30);
        actions.moveToElement("navigation.LatestPosts");
        actions.waitForElementVisible("posts.NewPost", 30);
        actions.clickElement("posts.NewPost");
        actions.waitMillis(500);
    }

    public void setVisibility(){
        actions.waitForElementVisible("post.VisibilityMenu",30);
        actions.clickElement("post.VisibilityMenu");
        actions.waitMillis(500);
        actions.waitForElementVisible("post.VisibilityPublic",30);
        actions.clickElement("post.VisibilityPublic");
        actions.waitMillis(500);
    }

    public void enterFile(String fileDir){
        actions.waitForElementVisible("post.File", 30);
        actions.typeValueInField(UserActions.getTextWithKey(fileDir), "post.File");
        actions.waitMillis(500);
    }

    public void enterTitle(String title){
        actions.waitForElementVisible("post.Title", 500);
        actions.typeValueInField(UserActions.getTextWithKey(title), "post.Title");
        actions.waitMillis(500);
    }

    public void assertPostPresent(String text){
        actions.assertTextExists(UserActions.getTextWithKey(text));
    }

    public void assertPostNotPresent(String text){
        actions.assertTextNotExists(UserActions.getTextWithKey(text));
    }

    public void saveChanges(){
        actions.waitForElementVisible("button.Save", 30);
        actions.clickElement("button.Save");
        actions.waitMillis(500);
    }
}
