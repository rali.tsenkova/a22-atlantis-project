package com.telerikacademy.finalproject.pages;

public class PostsPage extends BasePage {
    public PostsPage() {
        super("posts.url");
    }

    public void editComment(String text){
        actions.waitForElementVisible("editComments.Button", 30);
        actions.clickElement("editComments.Button");
        actions.waitMillis(500);
        actions.waitForElementVisible("editComments.Textarea", 30);
        actions.typeValueInField(text, "editComments.Textarea");
        actions.waitMillis(500);
        actions.clickElement("editComments.SubmitButton");
        actions.waitMillis(500);
    }
    public void deleteComment(){
        actions.clickElement("editComments.Button");
        actions.waitMillis(500);
        actions.clickElement("commentDelete.Button");
        actions.waitMillis(500);
    }
    public void editPostByAddingCategory(){
        actions.waitForElementVisible("postEdit.Button", 30);
        actions.clickElement("postEdit.Button");
        actions.waitForElementVisible("post.CategoryBox",30);
        actions.clickElement("post.CategoryBox");
        actions.clickElement("postSelectCategory.Option");
        actions.waitForElementVisible("post.SaveButton",30);
        actions.clickElement("post.SaveButton");
    }
    public void editPost(){
        actions.waitForElementVisible("postEdit.Button", 30);
        actions.clickElement("postEdit.Button");
        actions.waitMillis(500);
    }
}