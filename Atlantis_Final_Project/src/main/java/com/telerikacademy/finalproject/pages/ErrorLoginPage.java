package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class ErrorLoginPage extends BasePage {
    public ErrorLoginPage(){
        super("errorLogin.url");
    }
    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.contains(url));
    }
}

