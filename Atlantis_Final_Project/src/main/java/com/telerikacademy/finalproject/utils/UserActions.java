package com.telerikacademy.finalproject.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class UserActions {
    public static final String IMAGE_PATH = "image.path";

    public static final int WAIT_TIME_SECONDS = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        element.click();
    }

    public void clickFirstElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        List<WebElement> elements = driver.findElements(By.xpath(locator));
        WebElement element = elements.get(0);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        element = wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        element.sendKeys(value);
    }

    public void typeValueInField2(String valueKey, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        String value = Utils.getResourcePropertyByKey(valueKey);
        element.sendKeys(value);
    }

    public void typeRandomValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        value += RandomStringUtils.randomAlphanumeric(5);
        element.sendKeys(value);
    }

    public void typeImageInField(String image, String field, Object... fieldArguments) {
        String imagePath = Utils.getConfigPropertyByKey(IMAGE_PATH);
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_SECONDS);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        String fullImagePath = imagePath + image;
        element.sendKeys(fullImagePath);
    }

    public void waitMillis(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clearTextField(String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();

    }
    public void moveToElement(String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public static String getTextWithKey(String key){
        return Utils.getResourcePropertyByKey(key);
    }

    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator,int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void waitForElementVisible(String locator, int seconds){
        WebDriverWait wait = new WebDriverWait(driver,seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
    public void assertElementClickable(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Assert.assertNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertTextExists(String text){
        Assert.assertTrue("Text not found.", driver.getPageSource().contains(text));
    }

    public void assertTextNotExists(String text){
        Assert.assertFalse("Text is found.", driver.getPageSource().contains(text));
    }
}
