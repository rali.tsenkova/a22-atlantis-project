package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UpdatePostsAdminTests extends BaseTest {
    private LoginPage loginPage = new LoginPage();
    private static final String VALID_LOGIN = "atlantis.final.project@gmail.com";
    private static final String VALID_PASSWORD = "Alabal@123";
    private LogoutPage logoutPage = new LogoutPage();
    private NavigationPage navigationPage = new NavigationPage();
    private PostsPage postsPage = new PostsPage();
    private PostsAdminPage postsAdminPage = new PostsAdminPage();
    private UpdatePostPage updatePostPage = new UpdatePostPage();
    private DeletePostPage deletePostPage = new DeletePostPage();
    private UserProfilePage userProfilePage = new UserProfilePage();


    @Before
    public void before() {
        loginPage.navigateToPage();
        loginPage.login(VALID_LOGIN, VALID_PASSWORD);
    }

    @After
    public void after() {
        logoutPage.logout();
    }

    @Test
    public void test304_editPostByAddingCategory() {
        postsAdminPage.navigateToPage();
        postsAdminPage.clickOnPost();
        postsPage.assertPageNavigated();
        postsPage.editPostByAddingCategory();
        postsPage.assertPageNavigated();
    }

    @Test
    public void test308_deletePost() {
        postsAdminPage.navigateToPage();
        postsAdminPage.clickOnPost();
        postsPage.assertPageNavigated();
        postsPage.editPost();
        updatePostPage.assertPageNavigated();
        updatePostPage.clickDeletePost();
        deletePostPage.deletePost();
        userProfilePage.assertPageNavigated();
    }
}
