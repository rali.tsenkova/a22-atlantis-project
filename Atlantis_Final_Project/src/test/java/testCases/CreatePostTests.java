package testCases;

import com.telerikacademy.finalproject.pages.CreatePostPage;
import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.LogoutPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreatePostTests extends BaseTest {
    private static final CreatePostPage postPage = new CreatePostPage();
    private static final LoginPage login = new LoginPage();
    private static final LogoutPage logout = new LogoutPage();

    @BeforeClass
    public static void testsInit(){
        login.navigateToPage();
        login.login(UserActions.getTextWithKey("regularUser.username"),
                UserActions.getTextWithKey("regularUser.password"));
    }

    @AfterClass
    public static void testsTearDown(){
        logout.logout();
    }

    @Test
    public void test110_createPostWithPicture(){
        postPage.openCreateNewPost();
        postPage.setVisibility();
        postPage.enterFile("postWithPicture.dir");
        postPage.enterTitle("postWithPicture.title");
        postPage.saveChanges();
        postPage.assertPostPresent("postWithPicture.title");
    }

    @Test
    public void test111_createPostWithoutPicture(){
        postPage.openCreateNewPost();
        postPage.setVisibility();
        postPage.enterTitle("postWithoutPicture.title");
        postPage.saveChanges();
        postPage.assertPostNotPresent("postWithoutPicture.title");
    }

//    ### This test do not pass because video file is too large, but there is no message about file size
//    ### Issue #3
//    @Test
//    public void test112_createPostWithVideo(){
//        postPage.openCreateNewPost();
//        postPage.setVisibility();
//        postPage.enterFile("postWithVideo.dir");
//        postPage.enterTitle("postWithVideo.title");
//        postPage.saveChanges();
//        postPage.assertPostPresent("postWithVideo.dir");
//    }
}
