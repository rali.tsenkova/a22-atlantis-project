package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriesAdminTests extends BaseTest {
    private LoginPage loginPage = new LoginPage();
    private static final String VALID_LOGIN = "atlantis.final.project@gmail.com";
    private static final String VALID_PASSWORD = "Alabal@123";
    private LogoutPage logoutPage = new LogoutPage();
    private NavigationPage navigationPage = new NavigationPage();
    private CategoriesAdminPage categoriesAdminPage = new CategoriesAdminPage();
    private UpdateCategoryPage updateCategoryPage = new UpdateCategoryPage();
    private static final String CATEGORY_IMAGE = "sport.png";
    private CategoryNewPage categoryNewPage = new CategoryNewPage();
    private DeleteCategoryPage deleteCategoryPage = new DeleteCategoryPage();

    @Before
    public void before() {
        loginPage.navigateToPage();
        loginPage.login(VALID_LOGIN, VALID_PASSWORD);
    }

    @After
    public void after() {
        logoutPage.logout();
    }

    @Test
    public void test311_addCategory() {
        categoriesAdminPage.navigateToPage();
        categoriesAdminPage.clickAddCategory();
        categoryNewPage.assertPageNavigated();
        categoryNewPage.createCategory(CATEGORY_IMAGE, "New_");
        categoriesAdminPage.assertPageNavigated();
    }

    @Test
    public void test315_deleteCategory() {
        categoriesAdminPage.navigateToPage();
        updateCategoryPage.editCategory();
        deleteCategoryPage.assertPageNavigated();
        deleteCategoryPage.deleteCategory();
        categoriesAdminPage.assertPageNavigated();
    }
}
