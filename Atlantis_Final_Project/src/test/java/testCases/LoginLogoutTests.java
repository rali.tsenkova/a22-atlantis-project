package testCases;

import com.telerikacademy.finalproject.pages.ErrorLoginPage;
import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.LogoutPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginLogoutTests extends BaseTest{
    private static final String invalidLogin = "sladb12345+2@gmail.com";
    private static final String invalidPassword = "TestUser12";
    private LoginPage loginPage = new LoginPage();
    private static long WAIT_TIME = 1000;
    private static final String validLogin = "atlantis.final.project@gmail.com";
    private static final String validPassword = "Alabal@123";
    private NavigationPage navigationPage = new NavigationPage();
    private LogoutPage logoutPage = new LogoutPage();
    private ErrorLoginPage errorLoginPage = new ErrorLoginPage();

    @Before
    public void before(){
        loginPage.navigateToPage();
    }

    @Test
    public void test200_loginWithInvalidCredentials(){
        loginPage.login(invalidLogin,invalidPassword);
        errorLoginPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Test
    public void test201_loginWithoutCredentials(){
        loginPage.login("","");
        errorLoginPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Test
    public void test203_204_loginWithValidCredentialsAndLogout(){
        loginPage.login(validLogin,validPassword);
        navigationPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
        logoutPage.logout();
        logoutPage.assertPageNavigated();
    }
}
