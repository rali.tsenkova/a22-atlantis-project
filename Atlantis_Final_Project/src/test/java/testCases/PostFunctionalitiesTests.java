package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.LogoutPage;
import com.telerikacademy.finalproject.pages.PostFunctionalitiesPage;
import com.telerikacademy.finalproject.pages.UpdateUserProfilePage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;

public class PostFunctionalitiesTests extends BaseTest {
    private static final PostFunctionalitiesPage post = new PostFunctionalitiesPage();
    private static final LoginPage login = new LoginPage();
    private static final LogoutPage logout = new LogoutPage();

    @BeforeClass
    public static void testClassInit(){
        login.navigateToPage();
        login.login(UserActions.getTextWithKey("regularUser.username"),
                UserActions.getTextWithKey("regularUser.password"));
    }

    @AfterClass
    public static void testClassTearDown(){
        logout.logout();
    }

    @Before
    public void testInit(){
        post.openAllPostSection();
    }

    @Test
    public void test120_searchForPost(){
        post.searchForPostByTitle("post.SearchTitle");
        post.assertPostFound("post.SearchTitle");
    }

    @Test
    public void test121_likeDislikePost(){
        post.searchForPostByTitle("post.LikeDislikeTitle");
        post.openPost();
        post.likePost();
        post.assertPostLiked();
        post.dislikePost();
        post.assertPostDisliked();
    }

    @Test
    public void test123_commentPost(){
        post.searchForPostByTitle("post.LeaveCommentTitle");
        post.openPost();
        post.leaveComment("post.LeaveCommentText");
        post.assertCommentLeft("post.LeaveCommentText");
    }
}
