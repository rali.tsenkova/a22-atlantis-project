package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UpdateCommentsTests extends BaseTest {
    private LoginPage loginPage = new LoginPage();
    private static final String VALID_LOGIN = "atlantis.final.project@gmail.com";
    private static final String VALID_PASSWORD = "Alabal@123";
    private LogoutPage logoutPage = new LogoutPage();
    private NavigationPage navigationPage = new NavigationPage();
    private PostsPage postsPage = new PostsPage();
    private PostsAdminPage postsAdminPage = new PostsAdminPage();
    private DeleteCommentPage deleteCommentPage = new DeleteCommentPage();

    @Before
    public void before(){
        loginPage.navigateToPage();
        loginPage.login(VALID_LOGIN, VALID_PASSWORD);
    }

    @After
    public void after(){
        logoutPage.logout();
    }

    @Test
    public void test300_editCommentAdminUser(){
        postsAdminPage.navigateToPage();
        postsAdminPage.clickPostWithComment();
        postsPage.assertPageNavigated();
        postsPage.editComment("new");
        postsPage.assertPageNavigated();
    }

    @Test
    public void test302_deleteCommentsAdminUser(){
        postsAdminPage.navigateToPage();
        postsAdminPage.clickPostWithComment();
        postsPage.assertPageNavigated();
        postsPage.deleteComment();
        deleteCommentPage.assertPageNavigated();
        deleteCommentPage.confirmDeleteComment();
        postsPage.assertPageNavigated();
    }

    // This test should fail as there is a bug with page redirection when you cancel to delete a comment, issue #2
    @Test
    public void test301_cancelToDeleteCommentAdminUser(){
        postsAdminPage.navigateToPage();
        postsAdminPage.clickPostWithComment();
        postsPage.assertPageNavigated();
        postsPage.deleteComment();
        deleteCommentPage.cancelToConfirmDeleteComment();
        postsPage.assertPageNavigated();
    }
}
