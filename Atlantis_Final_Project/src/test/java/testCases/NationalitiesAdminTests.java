package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NationalitiesAdminTests extends BaseTest {
    private LoginPage loginPage = new LoginPage();
    private static final String VALID_LOGIN = "atlantis.final.project@gmail.com";
    private static final String VALID_PASSWORD = "Alabal@123";
    private LogoutPage logoutPage = new LogoutPage();
    private NavigationPage navigationPage = new NavigationPage();
    private NationalitiesAdminPage nationalitiesAdminPage = new NationalitiesAdminPage();
    private static final String NATIONALITY_NAME = "Natio_";
    private UpdateNationalityPage updateNationalityPage = new UpdateNationalityPage();
    private DeleteNationalityPage deleteNationalityPage = new DeleteNationalityPage();

    @Before
    public void before() {
        loginPage.navigateToPage();
        loginPage.login(VALID_LOGIN, VALID_PASSWORD);
    }

    @After
    public void after() {
        logoutPage.logout();
    }

    @Test
    public void test316_addNationality(){
        nationalitiesAdminPage.navigateToPage();
        nationalitiesAdminPage.addNationality(NATIONALITY_NAME);
        nationalitiesAdminPage.assertPageNavigated();
    }

    @Test
    public void test318_deleteNationality(){
        nationalitiesAdminPage.navigateToPage();
        updateNationalityPage.editNationality();
        deleteNationalityPage.assertPageNavigated();
        deleteNationalityPage.deleteNationality();
        nationalitiesAdminPage.assertPageNavigated();
    }
}
