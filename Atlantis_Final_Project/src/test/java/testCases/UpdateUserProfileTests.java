package testCases;

import com.telerikacademy.finalproject.pages.CreatePostPage;
import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.LogoutPage;
import com.telerikacademy.finalproject.pages.UpdateUserProfilePage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UpdateUserProfileTests extends BaseTest {
    private static final UpdateUserProfilePage profilePage = new UpdateUserProfilePage();
    private static final LoginPage login = new LoginPage();
    private static final LogoutPage logout = new LogoutPage();

    @BeforeClass
    public static void testsInit(){
        login.navigateToPage();
        login.login(UserActions.getTextWithKey("regularUser.username"),
                UserActions.getTextWithKey("regularUser.password"));
    }

    @AfterClass
    public static void testsTearDown(){
        profilePage.clearData();
        logout.logout();
    }

    @Test
    public void test100_changeProfilePictureVisibility(){
        profilePage.openEditProfile();
        profilePage.editProfilePictureVisibilityToConnections();
        profilePage.saveChanges();
        profilePage.assertChangeVisibility();
    }

    @Test
    public void test101_changeUserLastName(){
        profilePage.openEditProfile();
        profilePage.enterNewLastName("update.NewLastName");
        profilePage.saveChanges();
        profilePage.assertChangeLastName("update.NewLastName");
    }

//    @Test
//    public void test102_changePassword(){
//        profilePage.openEditProfile();
//        profilePage.clickOnChangePasswordButton();
//        profilePage.changePassword("regularUser.password", "update.newPassword");
//        profilePage.saveChanges();
//        profilePage.assertChangePassword();
//    }
}
