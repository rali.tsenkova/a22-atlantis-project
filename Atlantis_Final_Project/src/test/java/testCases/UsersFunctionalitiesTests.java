package testCases;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.*;

public class UsersFunctionalitiesTests extends BaseTest {
    private static final UpdateUserProfilePage profilePage = new UpdateUserProfilePage();
    private static final UsersFunctionalitiesPage user = new UsersFunctionalitiesPage();
    private static final LoginPage login = new LoginPage();
    private static final LogoutPage logout = new LogoutPage();

    @BeforeClass
    public static void testsInit(){
        login.navigateToPage();
        login.login(UserActions.getTextWithKey("regularUser.username"),
                UserActions.getTextWithKey("regularUser.password"));
    }

    @AfterClass
    public static void testsTearDown(){
        logout.logout();
    }
    @Before
    public void testInit(){
        user.openAllUsersSection();
    }

    @Test
    public void test130_searchForExistingUser(){
        user.searchForUserByEmail("email.registeredUserEmail");
        user.assertUserFound("email.registeredUserEmail");
    }

    @Test
    public void test131_sendRequestToExistingUser(){
        user.searchForUserByEmail("email.registeredUserEmail");
        user.openUserProfile();
        user.sendConnectionRequest();
        user.assertRequestSent();
    }

}
