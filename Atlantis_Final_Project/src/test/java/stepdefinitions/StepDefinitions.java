package stepdefinitions;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions {
    private static long WAIT_TIME = 1000;
    private LoginPage loginPage = new LoginPage();
    private ErrorLoginPage errorLoginPage = new ErrorLoginPage();
    private LogoutPage logoutPage = new LogoutPage();
    private NavigationPage navigationPage = new NavigationPage();
    private PostsPage postsPage = new PostsPage();
    private PostsAdminPage postsAdminPage = new PostsAdminPage();
    private DeleteCommentPage deleteCommentPage = new DeleteCommentPage();
    private CategoriesAdminPage categoriesAdminPage = new CategoriesAdminPage();
    private CategoryNewPage categoryNewPage = new CategoryNewPage();
    private UpdateCategoryPage updateCategoryPage = new UpdateCategoryPage();
    private DeleteCategoryPage deleteCategoryPage = new DeleteCategoryPage();
    private DeletePostPage deletePostPage = new DeletePostPage();
    private UserProfilePage userProfilePage = new UserProfilePage();
    private NationalitiesAdminPage nationalitiesAdminPage = new NationalitiesAdminPage();
    private UpdateNationalityPage updateNationalityPage = new UpdateNationalityPage();
    private DeleteNationalityPage deleteNationalityPage = new DeleteNationalityPage();
    private UpdatePostPage updatePostPage = new UpdatePostPage();

    private final UpdateUserProfilePage user = new UpdateUserProfilePage();
    private final CreatePostPage post = new CreatePostPage();
    private final UsersFunctionalitiesPage userFunction = new UsersFunctionalitiesPage();
    private final PostFunctionalitiesPage postFunction = new PostFunctionalitiesPage();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
        actions.waitMillis(WAIT_TIME);
    }

    @Given("ClickFirst $element element")
    @When("ClickFirst $element element")
    @Then("ClickFirst $element element")
    public void clickFirstElement(String element) {
        actions.clickFirstElement(element);
        actions.waitMillis(WAIT_TIME);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field) {
        actions.typeValueInField(value, field);
        actions.waitMillis(WAIT_TIME);
    }

    @Given("TypeImage $value in $name field")
    @When("TypeImage $value in $name field")
    @Then("TypeImage $value in $name field")
    public void typeImageInField(String value, String field) {
        actions.typeImageInField(value, field);
        actions.waitMillis(WAIT_TIME);
    }

    @Given("TypeRandom $value in $name field")
    @When("TypeRandom $value in $name field")
    @Then("TypeRandom $value in $name field")
    public void typeRandomInField(String value, String field) {
        actions.typeRandomValueInField(value, field);
        actions.waitMillis(WAIT_TIME);
    }


    @Given("I navigate to Login page")
    @When("I navigate to Login page")
    @Then("I navigate to Login page")
    public void checkLoginPage() {
        loginPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to ErrorLoginPage page")
    @When("I navigate to ErrorLoginPage page")
    @Then("I navigate to ErrorLoginPage page")
    public void checkErrorLoginPage() {
        errorLoginPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Navigation page")
    @When("I navigate to Navigation page")
    @Then("I navigate to Navigation page")
    public void checkNavigationPage() {
        navigationPage.assertPageNavigated();
    }

    @Given("I can see $key element")
    @When("I can see $key element")
    @Then("I can see $key element")
    public void checkElementPresent(String key) {
        actions.assertElementPresent(key);
        actions.waitMillis(WAIT_TIME);
        actions.assertElementClickable(key);
    }

    @Given("I navigate to Logout page")
    @When("I navigate to Logout page")
    @Then("I navigate to Logout page")
    public void checkLogoutPage() {
        logoutPage.assertPageNavigated();
    }

    @Given("I navigate to Posts page")
    @When("I navigate to Posts page")
    @Then("I navigate to Posts page")
    public void checkPostsPage() {
        postsPage.assertPageNavigated();
    }

    @Given("I navigate to Admin Posts page")
    @When("I navigate to Admin Posts page")
    @Then("I navigate to Admin Posts page")
    public void checkAdminPostsPage() {
        postsAdminPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Update Posts page")
    @When("I navigate to Update Posts page")
    @Then("I navigate to Update Posts page")
    public void updatePostPage() {
        updatePostPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Delete Posts page")
    @When("I navigate to Delete Posts page")
    @Then("I navigate to Delete Posts page")
    public void deletePostPage() {
        deletePostPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to DeleteCommentPage page")
    @When("I navigate to DeleteCommentPage page")
    @Then("I navigate to DeleteCommentPage page")
    public void deleteCommentPage() {
        deleteCommentPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Admin Categories page")
    @When("I navigate to Admin Categories page")
    @Then("I navigate to Admin Categories page")
    public void checkAdminCategoriesPage() {
        categoriesAdminPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Category New page")
    @When("I navigate to Category New page")
    @Then("I navigate to Category New page")
    public void checkCategoryNewPage() {
        categoryNewPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Update Category page")
    @When("I navigate to Update Category page")
    @Then("I navigate to Update Category page")
    public void updateCategoryPage() {
        updateCategoryPage.assertPageNavigated();
    }

    @Given("I navigate to Delete Category page")
    @When("I navigate to Delete Category page")
    @Then("I navigate to Delete Category page")
    public void deleteCategoryPage() {
        deleteCategoryPage.assertPageNavigated();
    }

    @Given("I navigate to User Profile page")
    @When("I navigate to User Profile page")
    @Then("I navigate to User Profile page")
    public void userProfilePage() {
        navigationPage.assertPageNavigated();
    }
    @Given("I navigate to Admin Nationalities page")
    @When("I navigate to Admin Nationalities page")
    @Then("I navigate to Admin Nationalities page")
    public void adminNationalitiesPagePage() {
        nationalitiesAdminPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("I navigate to Update Nationality page")
    @When("I navigate to Update Nationality page")
    @Then("I navigate to Update Nationality page")
    public void updateNationalityPage() {
        updateNationalityPage.assertPageNavigated();
    }

    @Given("I navigate to Delete Nationality page")
    @When("I navigate to Delete Nationality page")
    @Then("I navigate to Delete Nationality page")
    public void deleteNationalityPage() {
        deleteNationalityPage.assertPageNavigated();
        actions.waitMillis(WAIT_TIME);
    }

    @Given("Enter username $user and password $pass")
    @When("Enter username $user and password $pass")
    @Then("Enter username $user and password $pass")
    public void loginUser(String user, String pass) {
        loginPage.login(UserActions.getTextWithKey(user), UserActions.getTextWithKey(pass));
    }

    @Given("Start creating new post")
    @When("Start creating new post")
    @Then("Start creating new post")
    public void openNewPostLink() {
        post.openCreateNewPost();
    }

    @Given("Set post picture visibility to public")
    @When("Set post picture visibility to public")
    @Then("Set post picture visibility to public")
    public void setPostPicVisibility() {
        post.setVisibility();
    }

    @Given("Enter title $title")
    @When("Enter title $title")
    @Then("Enter title $title")
    public void enterTitle(String title) {
        post.enterTitle(title);
    }

    @Given("Enter file directory $dir")
    @When("Enter file directory $dir")
    @Then("Enter file directory $dir")
    public void enterFileDir(String dir) {
        post.enterFile(dir);
    }

    @Given("Open all posts")
    @When("Open all posts")
    @Then("Open all posts")
    public void openAllPosts() {
        postFunction.openAllPostSection();
    }

    @Given("Search for post with title $title")
    @When("Search for post with title $title")
    @Then("Search for post with title $title")
    public void searchForPostTitle(String title) {
        postFunction.searchForPostByTitle(title);
    }

    @Given("Post with title $title is found")
    @When("Post with title $title is found")
    @Then("Post with title $title is found")
    public void assertPostFound(String title) {
        postFunction.assertPostFound(title);
    }

    @Given("Open post founded")
    @When("Open post founded")
    @Then("Open post founded")
    public void openPostFound() {
        postFunction.openPost();
    }

    @Given("Like post")
    @When("Like post")
    @Then("Like post")
    public void likePostFound() {
        postFunction.likePost();
    }

    @Given("Dislike post")
    @When("Dislike post")
    @Then("Dislike post")
    public void dislikePostFound() {
        postFunction.dislikePost();
    }

    @Given("Assert post liked")
    @When("Assert post liked")
    @Then("Assert post liked")
    public void assertPostLiked() {
        postFunction.assertPostLiked();
    }

    @Given("Assert post disliked")
    @When("Assert post disliked")
    @Then("Assert post disliked")
    public void assertPostDisliked() {
        postFunction.assertPostDisliked();
    }

    @Given("Leave a comment with text $text")
    @When("Leave a comment with text $text")
    @Then("Leave a comment with text $text")
    public void leaveComment(String text) {
        postFunction.leaveComment(text);
    }

    @Given("Assert comment $text is left")
    @When("Assert comment $text is left")
    @Then("Assert comment $text is left")
    public void assertCommentLeft(String text) {
        postFunction.assertCommentLeft(text);
    }

    @Given("Open User section")
    @When("Open User section")
    @Then("Open User section")
    public void openUsersSection() {
        userFunction.openAllUsersSection();
    }

    @Given("Search for user with email $email")
    @When("Search for user with email $email")
    @Then("Search for user with email $email")
    public void searchForUser(String mail) {
        userFunction.searchForUserByEmail(mail);
    }

    @Given("User $mail is found")
    @When("User $mail is found")
    @Then("User $mail is found")
    public void assertUserFound(String mail) {
        userFunction.assertUserFound(mail);
    }

    @Given("Open user founded profile")
    @When("Open user founded profile")
    @Then("Open user founded profile")
    public void openProfile() {
        userFunction.openUserProfile();
    }

    @Given("Send connection request")
    @When("Send connection request")
    @Then("Send connection request")
    public void sendRequest() {
        userFunction.sendConnectionRequest();
    }

    @Given("Request it sent")
    @When("Request it sent")
    @Then("Request it sent")
    public void assertSentRequest() {
        userFunction.sendConnectionRequest();
    }

    @Given("Post $element is present")
    @When("Post $element is present")
    @Then("Post $element is present")
    public void assertPostCreated(String element) {
        post.assertPostPresent(element);
    }

    @Given("Element $element is not present")
    @When("Element $element is not present")
    @Then("Element $element is not present")
    public void assertPostNotCreated(String key) {
        actions.assertElementNotPresent(key);
    }

    @Given("Element $element present")
    @When("Element $element present")
    @Then("Element $element present")
    public void assertElementPresent(String element) {
        actions.assertElementPresent(element);
    }

    @Given("Open edit profile")
    @When("Open edit profile")
    @Then("Open edit profile")
    public void openEditProfile() {
        user.openEditProfile();
    }

//    @Given("Choose to change password")
//    @When("Choose to change password")
//    @Then("Choose to change password")
//    public void clickChangePassword() {
//        user.clickOnChangePasswordButton();
//    }
//    @Given("Change password from $old to $new")
//    @When("Change password from $old to $new")
//    @Then("Change password from $old to $new")
//    public void changePassword(String oldPass, String newPass) {
//        user.changePassword(oldPass, newPass);
//    }

    @Given("Profile picture visibility is changed")
    @When("Profile picture visibility is changed")
    @Then("Profile picture visibility is changed")
    public void assertVisibilityChanged() {
        user.assertChangeVisibility();
    }

    @Given("Change profile picture visibility")
    @When("Change profile picture visibility")
    @Then("Change profile picture visibility")
    public void changeVisibility() {
        user.editProfilePictureVisibilityToConnections();
    }

    @Given("Edit last name to $name")
    @When("Edit last name to $name")
    @Then("Edit last name to $name")
    public void editLastName(String name) {
        user.enterNewLastName(name);
    }

    @Given("Assert name changed to $name")
    @When("Assert name changed to $name")
    @Then("Assert name changed to $name")
    public void assertNameChanged(String name) {
        user.assertChangeLastName(name);
    }

    @Given("Roll back data")
    @When("Roll back data")
    @Then("Roll back data")
    public void rollbackData() {
        user.clearData();
    }
}