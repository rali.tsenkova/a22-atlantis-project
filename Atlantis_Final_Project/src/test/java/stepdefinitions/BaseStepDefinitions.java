package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.*;

public class BaseStepDefinitions {
    protected UserActions actions = new UserActions();
    @BeforeScenario
    public void beforeStories(){
        UserActions.loadBrowser("base.url");
    }

    @AfterScenario
    @AfterStory
    public void safeLogout() {
        try {
            actions.clickElement("logout.LogoutButton");
        } catch (Exception ex) {/**do nothing if logout fails**/}
    }

    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }
}
