Meta:
@userCreatePost

Narrative:
As a registered user I can create post with attached picture or video, but can not create post without attached file.

Scenario: Create a post with attached picture

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Start creating new post
And Set post picture visibility to public
And Enter title postWithPicture.title
And Enter file directory postWithPicture.dir
And Click button.Save element
Then Post postWithPicture.title is present


Scenario: Create a post without attached picture

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Start creating new post
And Set post picture visibility to public
And Enter title postWithoutPicture.title
And Click button.Save element
Then Element postWithoutPicture.title is not present


Scenario: Create a post with attached video file
Meta:
@skip

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Start creating new post
And Set post picture visibility to public
And Enter title postWithVideo.title
And Enter file directory postWithVideo.dir
And Click button.Save element
Then Post postWithVideo.title is present