Meta:
@userFunctionalities

Narrative:
As a registered user I can search for an existing user and send a friend request

Scenario: Search for an existing user by email and send connection request

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Open User section
And Search for user with email email.registeredUserEmail
Then User email.registeredUserEmail is found
And Open user founded profile
And Send connection request
Then Request it sent