Meta:
@userUpdateProfile

Narrative:
As a registered user I can update my profile with changing profile picture visibility, last name and pasword

Scenario: Change profile picture visibility and last name

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Open edit profile
And Change profile picture visibility
And Edit last name to update.NewLastName
And Click button.Save element
And Assert name changed to update.NewLastName
And Open edit profile
Then Profile picture visibility is changed