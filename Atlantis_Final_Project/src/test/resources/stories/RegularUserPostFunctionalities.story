Meta:
@userPostFunctionalities

Narrative:
As a registered user I can search for post, like post, dislike post, comment post

Scenario: Search for an existing post by title

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Open all posts
And Search for post with title post.SearchTitle
Then Post with title post.SearchTitle is found

Scenario: Like and dislike an existing post

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Open all posts
And Search for post with title post.LikeDislikeTitle
And Post with title post.LikeDislikeTitle is found
And Open post founded
And Like post
And Assert post liked
And Dislike post
Then Assert post disliked


Scenario: Comment an existing post

Given Click navigation.SignIn element
When I navigate to Login page
And Enter username regularUser.username and password regularUser.password
And Open all posts
And Search for post with title post.LeaveCommentTitle
And Post with title post.LeaveCommentTitle is found
And Open post founded
And Leave a comment with text post.LeaveCommentText
Then Assert comment post.LeaveCommentText is left