Meta: @adminComments

Narrative:
As an admin user I can delete profiles, edit, delete posts and commments


Scenario: Admin edit comment

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And Click sortComments.Button element
And ClickFirst postWithComment element
And I navigate to Posts page
And ClickFirst editComments.Button element
And Type new test in editComments.Textarea field
And Click editComments.SubmitButton element
And I navigate to Posts page
Then I can see logout.LogoutButton element

Scenario:Delete a comment

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And Click sortComments.Button element
And Click postWithComment element
And I navigate to Posts page
And Click editComments.Button element
And Click commentDelete.Button element
And I navigate to DeleteCommentPage page
And Click commentDeleteYes.Button element
And I navigate to Posts page
Then I can see logout.LogoutButton element

!--This test should fail as there is a bug with page redirection when you cancel to delete a comment, issue #2
Scenario:Cancel to delete a comment
Meta:@skip

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And Click sortComments.Button element
And Click postWithComment element
And I navigate to Posts page
And Click editComments.Button element
And Click commentDelete.Button element
And I navigate to DeleteCommentPage page
And Click commentDeleteNo.Button element
And I navigate to Navigation page
Then I can see logout.LogoutButton element
