Meta: @adminPosts

Narrative:
As an admin user I can edit and delete posts

Scenario: Admin edit post by changing visibility

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And ClickFirst postAdmin element
And Click postEdit.Button element
And Click post.VisibilityMenu element
And Click post.VisibilityConnections element
And Click post.SaveButton element
And I navigate to Posts page
Then I can see logout.LogoutButton element

Scenario: Admin edit post by adding category

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And ClickFirst postAdmin element
And Click postEdit.Button element
And I navigate to Update Posts page
And Click post.CategoryBox element
And Click postSelectCategory.Option element
And Click post.SaveButton element
And I navigate to Posts page
Then I can see logout.LogoutButton element

Scenario: Admin delete post

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminPosts.Button element
And I navigate to Admin Posts page
And ClickFirst postAdmin element
And Click postEdit.Button element
And I navigate to Update Posts page
And Click postDelete.Button element
And I navigate to Delete Posts page
And Click postDeleteYes.Button element
And I navigate to User Profile page
Then I can see logout.LogoutButton element
