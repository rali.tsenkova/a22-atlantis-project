Meta: @login

Narrative:
A user tries to log in with invalid credentials

Scenario: Login with invalid credentials
Given Click navigation.SignIn element
When I navigate to Login page
And Type sladb12345+2@gmail.com in login.Username field
And Type TestUser12 in login.Password field
And Click login.LoginButton element
Then I navigate to ErrorLoginPage page

Scenario: Login without filling in the login form
Given Click navigation.SignIn element
When I navigate to Login page
And Click login.LoginButton element
Then I navigate to ErrorLoginPage page

Scenario: Login with valid credentials
Given Click navigation.SignIn element
When I navigate to Login page
And Type sladb12345+2@gmail.com in login.Username field
And Type TestUser123@ in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
Then I can see logout.LogoutButton element

Scenario: Log out from user account
Given Click navigation.SignIn element
When I navigate to Login page
And Type sladb12345+2@gmail.com in login.Username field
And Type TestUser123@ in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And I can see logout.LogoutButton element
Then Click logout.LogoutButton element