Meta: @adminNationalities

Narrative:
As an admin user I create, edit and delete nationalities

Scenario: Admin add a new nationality

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminNationalities.Button element
And I navigate to Admin Nationalities page
And Click adminNationalitiesAdd.Button element
And TypeRandom Natio_ in inputNationality field
And Click nationalitySave.Button element
Then I navigate to Admin Nationalities page

Scenario: Admin delete an existing nationality

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminNationalities.Button element
And I navigate to Admin Nationalities page
And ClickFirst editNationality.Button element
And I navigate to Update Nationality page
And Click nationalityDelete.Button element
And I navigate to Delete Nationality page
And Click deleteNationalityYes.Button element
Then I navigate to Admin Nationalities page

