Meta: @anonymousUser

Narrative:
As an anonymous user can search for public profiles via name or e-mail

Scenario: An anonymous can search public user profile via name

Given I navigate to Navigation page
When I can see signUp.Button element
And I can see navigation.SignIn element
And Click users.Button element
And I navigate to User Profile page
And Type admin in input.Search field
And Click search.Button element
And I navigate to User Profile page
Then I can see userProfile.Result element

Scenario: An anonymous can search public user profile via e-mail

Given I navigate to Navigation page
When I can see signUp.Button element
And I can see navigation.SignIn element
And Click users.Button element
And I navigate to User Profile page
And Type atlantis.final.project@gmail.com in input.Search field
And Click search.Button element
And I navigate to User Profile page
Then I can see userProfile.Result element
