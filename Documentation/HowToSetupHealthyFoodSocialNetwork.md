# How to setup Healthy Food Social Network Project on Local Environment (localhost)  #

###  Register new gmail for admin tasks ###
- All members of the team can use it
- Avoid exposing personal account password in plain text
- `axxfinalprojectexample@gmail.com`
- Set `Access for less secure apps` to `Turn on` - https://procaresupport.com/gmail-less-secure-apps-setting/ needed in order to "send email to confirm registration" functionality to work.

### Use the  created above gmail email to register on https://cloudinary.com/
- Used in the app to manage uploaded images and videos

setup in Cloudinary
<your cloudinary cloud name>
<your cloudinary api key>
<your cloudinary api secret>

1. Download MySQL: go to https://dev.mysql.com/downloads/mysql
- For Mac users choose
 macOS 10.15 (x86, 64-bit), DMG Archive mysql-8.0.22-macos10.15-x86_64.dmg
- For Windows users choose:
Windows (x86, 32-bit), MSI Installer

2. Install the MySQL
- Double click on mysql-8.0.22-macos10.15-x86_64.dmg
- Accept  the default options
- Add new password (keep it save) or no password for the user root

3. Download MySQL Workbench (GUI sql client): go to https://dev.mysql.com/downloads/workbench
- Choose macOS (x86, 64-bit), DMG Archive (mysql-workbench-community-8.0.22-macos-x86_64.dmg)
- Choose Windows (x86, 64-bit), MSI Installer

4. Install MySQL Workbench
- Double click on mysql-workbench-community-8.0.22-macos-x86_64.dmg

5. Start MySQL database
For Mac go to Mac/System Preferences/MySQL and click start, it will ask  you for admin password

6. Open MySQL Workbench

7. Create new MySQL Connection:
hostname 127.0.0.1
port 3306
username root
password (the one provided during the installation of mysql) - click on "Store in Keychain..."(Windows: "Store in Vault") button and add the password

8. Once logged into MySQL create new schema, user and password:
CREATE DATABASE atlantis;
CREATE USER 'atlantis'@'localhost' IDENTIFIED BY 'atlantis';
GRANT ALL PRIVILEGES ON  .  TO 'atlantis'@'localhost';
FLUSH PRIVILEGES;

9. Import project sqls:
- Go to healthy-food-social-network/database/scripts
- Open create.sql with your text editor, copy  everything, paste it into MySQL Workbench and execute
- Open data.sql with your text editor, copy  everything, paste it into MySQL Workbench and execute

10. Download and install brew (https://www.datacamp.com/community/tutorials/homebrew-install-use)
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

11. Download and install gradle (for Windows add bin directory to PATH)
brew install gradle

12. Check gradle is installed
gradle -v

13. Update application.properties
Go to healthy-food-social-network/project/src/main/resources and open application.properties
cloudinary.cloud_name=<your cloudinary cloud name>
cloudinary.api_key=<your cloudinary api key>
cloudinary.api_secret=<your cloudinary api secret>
database.url=jdbc:mysql://localhost:3306/atlantis
database.username=atlantis
database.password=atlantis
spring.mail.username=<your email  address>
healthy.food.host=https://localhost:8080

14.Start application  server on local:
in your command prompt:
go to healthy-food-social-network/project folder
build the application:
./gradlew build -x test
start the application
./gradlew bootRun

- Wait for the application to start until you see messages like:
Tomcat started on port(s): 8080 (http) with context path ''
--- [  restartedMain] c.t.h.f.s.n.SocialNetworkApplication     : Started SocialNetworkApplication in 7.909 seconds (JVM running for 8.872)
or
<=========----> 75% EXECUTING [9h 38m 27s]                                                                              > :bootRun

15. Go to http://localhost:8080/register
register new user with some email (<YOUR_USER_EMAIL>)

16. Run the following sql in mysql bench to make the registered user above an admoin:
-- After the regstration you can promote the first user to admin:
INSERT INTO `atlantis`.`authorities` (`username`, `authority`)
VALUES ('<YOUR_USER_EMAIL>', 'ROLE_ADMIN');

17. Register also regular user

18. Run postman collection to create nessessary for the selenium tests data
(Postman\FilesToBeExecuted_Before_SELENIUM_TestRun\Run_Healthy_food_init_DB.bat)

19. Enjoy the  application

20. Go to the test project Atlantis_Final_Project/src/test/resources
open config.properties
add the following:

base.url=http://localhost:8080/
login.url=http://localhost:8080/login
logout.url=http://localhost:8080/login?logout
posts.url=http://localhost:8080/posts
adminPosts.url=http://localhost:8080/admin/posts
adminCategories.url=http://localhost:8080/admin/categories
adminCategoriesNew.url=http://localhost:8080/admin/categories/new
adminNationalities.url= http://localhost:8080/admin/nationalities
profile.url=http://localhost:8080/users/2
newPost.url=http://localhost:8080/posts/new
users.url=http://localhost:8080/users

21. make scripts executable
go to Atlantis_Final_Project/scripts folder and run in command shell
chmod +x runJBehaveTests.sh
chmod +x runSeleniumTests.sh

22. run the tests
go to Atlantis_Final_Project folder
run in command shell
./scripts/runJBehaveTests.sh
./scripts/runSeleniumTests.sh
