### Running Maven Test

1. Prerequisities
- Cloned repo
- Installed IntelliJ or another Java IDE
- Installed Maven
- Chrome browser
- Running application Healthy Food Social Network
2. How to run tests
- For BDD tests run file "runJBehaveTests.sh"
- For POM tests run file "runSeleniumTests.sh"
(Files can be found in a22-atlantis-project\Atlantis_Final_Project\scripts)

### Running REST Tests

1. Prerequisities
- Cloned repo
- Installed Postman
- Installed Node.js
- Installed newman and newman reporter
- Running application Healthy Food Social Network
2. How to run tests
- run file "Run_Healthy_food_tests_html.bat"
(File can be found in a22-atlantis-project\Postman\Ralitsa)
- run file "runRestApiTests.sh"
(File can be found in a22-atlantis-project\Postman\FilesToBeExecuted_Before_SELENIUM_TestRun)