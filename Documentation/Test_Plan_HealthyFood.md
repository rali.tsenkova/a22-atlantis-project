# Test Plan

### Testing Healthy Food Social Network

##### Prepared by: Ralitsa Georgieva and Slava Gagova
##### Date: 01.11.2020

#### Content:

#### 1. Introduction
#### 2. Test objectives
#### 3. Scope of testing
##### 3.1. Functionalities to be tested
##### 3.2. Functionalities not to be tested
#### 4. Testing strategy
#### 5. Deliverables
#### 6. Resources
##### 6.1. Human resources
##### 6.2. Hardware
##### 6.3. Environment requirements/ Tools
#### 7. Schedule
#### 8. Suspension criteria
#### 9. Exit Criteria


## 1. Introduction
The application under test is Healthy Food Social Network.
This thematical social network allows users to connect and search for each other.
There are two types of users - a regular user and an administrative user.
A regular user can create posts, leave comments, like/unlike posts, gets a feed of the latest posts of their connections, connects with other users.
An administrative user has the functionalities of the regular one and he can additionally edit and delete profiles, comments and posts.
The purpose of this project is to verify that the application fulfills the functional requirements.

## 2. Test objectives
The purpose of these testing activities is to:

- Verify if the requirements are fulfilled by the application
- Check if the application meets the expectations and needs of the customers
- Gain confidence in the quality of the product
- Identify and raise the defects in the application
## 3. Scope of testing
We will focus on main functionalities of the Healthy Food Social Network.
#### 3.1. Functionalities to be tested
- Create a user
- Update a user profile
- Log in / log out
- Test visibility of profile / posts
### *** Regular users *** ###
- Create a post
- Leave a comment
- Like / unlike post
- Upload picture
- Connect to other users
### *** Administrative user *** ###
- Edit / delete profiles
- Edit / delete post
- Edit / delete comments
#### 3.2. Functionalities not to be tested
Other functionalities of this application regarding communication and connectivity with other systems.
Compliance testing is out of scope.
## 4. Testing strategy
Exploratory testing will cover the main features and happy paths.
Testing approach will combine both manual and automaton testing as follows:
- Register new user - manual tests
- Login / Logout will be both manual and automated
- Some negative tests will be included for login with invalid credentials
- Public section and visibility can be tested manually. (We can think of automate a few tests.)
### *** Regular users *** ###
Compile manual tests and automation tests:
- Create a post
- Leave a comment
- Like / unlike post
- Upload picture
- Connect to other users
### *** Administrative user *** ###
Combine manual tests and automation tests
- Edit / delete profiles
- Edit / delete post
- Edit / delete comments
- Add /edit/ delete categories
- Add /edit/ delete nationalities
## 5. Deliverables
- Test cases
- Automated tests code / repository link
- Script for execution of automated tests
- A document with required prerequisites to run the tests
- A document with instructions how to run the tests
- High level test cases
- Test report
## 6. Resources
#### 6.1. Human resources
- Ralitsa Georgieva Junior QA
- Slava Gagova Junior QA

#### 6.2. Hardware
- MacBook Pro, macOS - version: 10.15.7, Processor: 2.3 GHz, Core i5, RAM 16 GB
- ASUS VivoBook, Windows 10 Home, Processor: 2.3 GHz, AMD RYZEN 7, RAM: 12 GB
#### 6.3. Environment requirements/ Tools
- IDE:IntelliJ
- Test Automation Design and Execution: Selenium
- Browsers: Chrome, FireFox, Safari
- Rest API Testing: Postman
- Source Code Management Tool: Gitlab
- Bug Tracking Tool: Gitlab
- Test Case: Excel Table
- Reporting: Document for the exploratory testing report, HTML reports for Selenium Tests, JBehave Tests and for Rest Tests executed via Postman
- Schedule and Progress Communicator: Trello
## 7. Schedule
- Test plan - 1 day
- Exploratory testing and report - 1 day
- Design of test cases - 2 days
- Set up test automation environment - 3 days
- Execution of manual testing - 3 days
- Log bugs - 2 days
- Implement Rest Api Automated Tests - 3 days
- Automated tests execution - 3 days
- Test report - 2 days
## 8. Suspension criteria:
- We stop testing if 50% of our tests fail
- The testing starts again when the development team fixes all the issues
## 9. Exit Criteria
- 90% of first priority tests pass;
- 80% of second priority test pass;
- 60% of third priority test pass.
