# Atlantis final project repo #

Ralitsa Georgieva & Slava Gagova

### Test Plan ###

- Content in the Documentation Folder

### Scripts ###

- Content in the Atlantis_Final_Project Folder

#### How to Setup Healthy Food Social Network Project on Local Environment (localhost) ####

- Content in the Documentation Folder

#### How to Run Tests ####

- Content in the Documentation Folder

### Schedule and Progress Communicator Trello:

https://trello.com/b/72lYru1i/qaproject-healthy-food-social-network

### Exploratory testing report ###

https://telerikacademy-my.sharepoint.com/:f:/p/slava_gagova_a22_learn/ErmhiqATZLJNhEfjVH8jrLYB9OBzYqzrAS8Ho6meGn4Xsw?e=dekyaY

### Test Cases ###

https://telerikacademy-my.sharepoint.com/:x:/p/slava_gagova_a22_learn/EXl_0_DfinxGma4431DE-WQB4v7ZTSmHmbMFMRgxhn_JPg?e=YIFbHX

### Logged Issues ###

https://gitlab.com/rali.tsenkova/a22-atlantis-project/-/issues

### Test Execution Reports ###

#### Summary and Charts ####

https://telerikacademy-my.sharepoint.com/:x:/p/slava_gagova_a22_learn/Ebo5DWkYxThGpJ4sNOnFw5MBV6NVv0AeDUA_G3qrKBl2rQ?e=MK43jp

#### Selenium Tests Report ####

https://telerikacademy-my.sharepoint.com/:b:/p/slava_gagova_a22_learn/EbPhQgsrx2pMhLpSCbZiqVAB_q3lnWQTY3QsVkRNfhfgEA?e=5kMdVd

#### JBehave Tests Report ####

https://telerikacademy-my.sharepoint.com/:b:/p/slava_gagova_a22_learn/EZEAmcdX_AVEjnDB1kwcAnoBOF2Y5GGScuLx_VyEAX-uNQ?e=Q99TIZ

#### Postman Reports ####

- Content in Postman folder

### Test Report ###

https://telerikacademy-my.sharepoint.com/:b:/p/slava_gagova_a22_learn/EfMHCIeKG7NGi9QWZy5OytcBnp7IykuSY1BIehCZtpQkgg?e=h0Z1ni
